#!/usr/bin/env sh

npm ci
npm run build:css
hugo --minify --cleanDestinationDir
