---
title: "About"
layout: about
draft: false
---

## About me

I've [published](https://dl.acm.org/doi/pdf/10.1145/3097983.3098078)
[research](https://arxiv.org/pdf/1806.10692.pdf) and
[worked](https://urbint.com/) [at](https://blueconduit.com)
[startups](https://rhizomedata.com) solving problems with technology for
utilities for the better part of a decade now. I'm going to do my best to
distill the useful parts of my experience, and publish them here in an effort to
reify my own thoughts and opinions about machine learning and technology.

I live in Brooklyn, where I play drums in a band and enjoy the company of my
wonderful partner Sophie.
