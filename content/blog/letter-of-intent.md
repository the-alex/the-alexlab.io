---
title: "Letter of Intent"
date: 2023-03-21T15:43:24-04:00
draft: true
---

Greetings! And welcome once again to my corner of the Internet. I started a blog in college, where I briefly wrote about technology and programming culture, probably well before I had any real perspective worth sharing. After a few years in industry, I became kind of embarrassed by it and deleted the posts. I've remained in dialog with my thoughts, but over several journaling, memo, and plain text repositories.

A lot has changed in the intervening years, and I find myself itching to publish my thoughts, if only to reify my ideas for friends close enough to understand me and value that perspective. I hope it encourages them to do the same.

So let's call this an informal _letter of intent._ I intend to write with clarity about my favorite ideas at the intersection of people, machines, and the interfaces we use to communicate. Things like decision intelligence, probabilistic modeling, data visualizations, data fusion, and the practical deployment of these programs so that others may find value in them.
