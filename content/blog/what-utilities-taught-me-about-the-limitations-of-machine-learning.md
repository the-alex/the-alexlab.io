---
title: What Utilities Taught Me About the Limitations of Machine Learning
date: 2023-04-03T16:59:07-04:00
abstract: Balancing resources, domain expertise, and machine learning to make high-stakes decisions, we must embrace the lessons of statistical modeling as the key to fairness, transparency, and accountability in consequential predictions.
draft: false
---


Throughout my career, I've been tasked with applying machine learning methods to inform human decision making. This started with finding lead pipes in the city of Flint using property data, allowing us to group risk into projects, and change our resource allocation as more pipe materials were revealed. This theme continued in my work at startups, where our teams used computer vision to prioritize the manual review of sewer pipe inspection videos, or time series forecasting to anticipate seasonal changes natural gas leaks, or using dig permit information to correlate project types and damage to natural gas assets, which informed the allocation of "watch and protect" advisory teams to risky dig sites.

These examples are united by a few notable characteristics. The first is they are resource allocation problems. If we had enough money, we'd just dig and inspect all the pipes. We'd inspect every sewer. We'd respond to every errant foul odor call. When constrained by budget and logistics, you must prioritize.

The second I'll highlight is that there is a great degree of domain knowledge in each of these problems. The plumbers who had worked in and around The City of Flint for decades before the crisis had broadly observed "where the lead was," and they had hypotheses that related building age and plumbing installation year to the incidence rate of lead pipes. This relationship of building era to construction materials is more true than we have time to go into.

The third, and maybe most important distinguishing characteristic, is the impact of these models on a consequential human decision. These models estimate the probability of high consequence events, and that influences the distribution of resources in a service territory. In Flint, that meant determining who's pipes would get replaced, and how soon. This has obvious implications for the fairness, transparency, and accountability in modeling practices. We need to be able to "explain" why a model arrived at a conclusion, often as table stakes for domain experts and regulators to trust the model.

I have come to understand that most useful applications of predictive modeling struggle against these same constraints, and that machine learning in general is ill-equipped to tackle them. Popular machine learning methods like boosted ensembles of decision trees, are easy and convenient methods of arriving at a reasonable predictor when the data is good and the relationships between features or their relative influence on outcomes is not obvious. However, this does not describe the majority of problems machine learning practitioners and data scientists are being asked to solve. The reality of the matter is that most practical prediction problems in the real world demand methods that implement an understanding of the data generating process, can be interpreted clearly by experts, and represent the underlying uncertainty in the estimates they produce.

So how did we get here? What's the reason for this mismatch between the problems we're trying to solve and the skilled labor deployed to solve them? Is the machine learning community adapting to meet the constraints of these domains? How far will these methods take us? And what does the alternative look like?

For everything, there are tradeoffs, but I'm here to make the case that statistical modeling is more accessible now to machine learning practitioners than ever before, and offers a more compelling modeling framework to meet the demands of fairness, transparency, and accountability in domains where predictions have real consequences.

I've been reading a lot over the last two years, and there's a lot worth sharing. Of course, this is all for me. If you're reading this, you must be a friend or coworker, because I don't expect to promote my work for some time. However, there are many other reasons to write, and here the most salient to me is to reify my own thoughts. Hopefully we get something coherent. In the mean time, here's a preview, via keywords and jargon, of what I've been reading.

- Box's model
- Causal inference
- Quasi-random experiments
- Difference in differences models
- Probabilistic programming
- Structural Equations Modeling
- Double Machine Learning
