---
title: "Does Machine Learning need Probabilistic Programming?"
date: 2023-03-23T15:48:06-04:00
abstract: >
    Exploring the benefits of probabilistic programming for ML practitioners, addressing limitations of current ML culture, and advocating for a more holistic approach that incorporates domain knowledge and interpretability.
draft: true
---

## Introduction

### My career in machine learning

Throughout my career, I've been tasked with applying machine learning methods to inform human decision making. This started with finding lead pipes in the city of Flint using tax lot data, allowing us to group risk into projects, and change our resource allocation as more pipe materials were revealed. This theme continued in my work at startups. I used computer vision to prioritize the manual review of sewer pipe inspection videos. I used time series forecasting to anticipate gas odor call volumes, and staff response centers appropriately. I used dig permit information to correlate project types and damage to natural gas assets, which informed the allocation of "watch and protect" teams to risky dig cites.

These examples are united by a few notable characteristics. The first is they are resource allocation problems. If we had enough money, we'd just dig and inspect all the pipes. We'd inspect every sewer. We'd respond to every errant foul odor call. When constrained by budget and logistics, you must prioritize.

The second I'll highlight is that there is a great degree of domain knowledge in each of these problems. The plumbers who had worked in and around The City of Flint for decades before the crisis had broadly observed "where the lead was," and they had hypotheses that related building age and plumbing installation year to the incidence rate of lead pipes. This relationship of building era to construction materials is more true than we have time to go into.

The third, and maybe most important, distinguishing characteristic, is the impact of these models on a consequential human decision. These models estimate the probability of high consequence events, and that influences the distribution of resources in a service territory. In Flint, that meant determining who's pipes would get replaced, and how soon. This has obvious implications for the fairness, transparency, and accountability in modeling practices. We need to be able to "explain" why a model arrived at a conclusion, often as table stakes for domain experts and regulators to trust the model.

I have come to understand that most useful applications of predictive modeling struggle against these same constraints, and that machine learning in general is ill-equipped to tackle them. Popular machine learning methods like boosted ensembles of decision trees, are easy and convenient methods of arriving at a reasonable predictor when the data is good and the relationships between features or their relative influence on outcomes is not obvious. However, this does not describe the majority of problems machine learning practitioners and data scientists are being asked to solve. The reality of the matter is that most practical prediction problems in the real world demand methods that implement an understanding of the data generating process, can be interpreted clearly by experts, and represent the underlying uncertainty in the estimates they produce.

So how did we get here? What's the reason for this mismatch between the problems we're trying to solve and the skilled labor deployed to solve them? Is the machine learning community adapting to meet the constraints of these domains? How far will these methods take us? And what does the alternative look like?

For everything, there are tradeoffs, but I'm here to make the case that statistical modeling is more accessible to machine learning practitioners than ever, and offers a more compelling modeling framework to meet the demands of fairness, transparency, and accountability in domains where predictions have real consequences.


### The deep learning hype wave

I came to college knowing I wanted to study computer science, but it wasn't until later that I became captivated by machine learning. The summer after my freshman year, I watched the Google I/O talk that introduced Google Photos. At one point in the demo, they demonstrate a new and incredible functionality: Semantic search.

I was only a freshman, but I knew enough about apps and the web to understand how the photos were synchronized between the phone and the datacenter. I even had a decent idea of how they did the UI layout and the challenges of rendering hundreds of photos as a user quickly scrolled. What absolutely captured me was the knowledge that, somewhere in the codebase, was a function that took a string and returned the index of every photo related to that string, and that was just magic to me. This, more than any other moment I can remember, galvanized an interest in machine learning.

This was a specific kind of machine learning. Indeed, it was the class of methods known as deep learning that afforded this breakthrough, and while the theory for efficiently fitting such models had been known for over twenty years, the aggregation of data and computing power available had limited the ability to productize the technology. Yet, suddenly, many problems that were previously assumed intractable, like those in computer vision an natural language processing, were seemingly trivial overnight.

This was the beginning of the last AI hype wave, and it spurred every technology company to adopt some kind of AI strategy. Today we see products powered by this kind of AI occupy the collective consciousness. Now we just expect Spotify's Discover Weekly to totally slap, and for Snapchat filters to freak everyone out. RF for Atari. There's a lot of AI pessimism, though, and this pessimism is most felt in the domains least served by these large models.

### Machine learning is outcome oriented

The culture of machine learning today is greatly influenced by it's origins as a method within the family of artificial intelligence. The focus of AI has always been about trying to give programs the ability to solve problems that are easy to humans and difficult for computers. In the 50's and 60's, this was playing chess and checkers, or being able to prove mathmatical theorems based on a few axioms. As programmatic as those tasks seem today, the emphasis was truly on the emulation of human abilities, and this fundamentally oriented machine learning toward outcomes. Where symbolic AI might have emphasized the representation of knowledge and the process of logical reasoning, it was still with the goal of creating a program that could perform human tasks convincingly, so the ability to interpret it's inner mechanical reasoning was secondary.

## The culture of statistics

## A hypothesis


---


## Introduction

### My career in machine learning

#### Resource allocation problems

#### Domain knowledge

#### Impact on consequential human decisions

### The deep learning hype wave

#### Captivating new technologies

#### AI hype and pessimism

### Machine learning is outcome oriented

## The culture of statistics

In contrast to machine learning, the culture of statistics is rooted in the rigorous analysis of data and the understanding of the underlying processes that generate it. While machine learning tends to prioritize performance and outcomes, statistics emphasizes the explanation and interpretation of data.

### Interpretability and uncertainty

Statistical models often provide a clearer understanding of the relationships between variables and the uncertainty associated with their predictions. This interpretability is particularly important when making consequential decisions based on model outputs.

### Collaboration with domain experts

Working with domain experts is an integral part of statistical modeling. Their knowledge can inform the development of more accurate and interpretable models, which are better suited to address real-world problems.

## A hypothesis

The increasing demand for fairness, transparency, and accountability in machine learning applications may lead to a shift in focus from performance-driven methods to more interpretable and explanatory statistical models. This shift may make statistical modeling more accessible and attractive to practitioners, ultimately leading to better outcomes in domains where predictions have real consequences.
